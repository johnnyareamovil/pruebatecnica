package com.example.pc.myapplication;

import android.app.Application;
import android.view.View;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by PC on 19/03/2018.
 */

public class MyApplication extends Application
{
    @Override
    public void onCreate() {
        super.onCreate();

        //Configuracion del Realm
        Realm.init(this);
        RealmConfiguration configuration = new RealmConfiguration.Builder().build();

        Realm.setDefaultConfiguration(configuration);
    }
}
