package com.example.pc.myapplication.db;

import com.example.pc.myapplication.di.AlCuadrado;

/**
 * Created by PC on 17/03/2018.
 */

public class AlCuadradoModel implements AlCuadrado.Model
{
    private AlCuadrado.Presenter presenter;
private double resultado;

    public AlCuadradoModel(AlCuadrado.Presenter presenter)
    {
        this.presenter = presenter;
    }

    @Override
    public void Alcuadrado(String data)
    {
        if(data.equals(""))
        {
            presenter.showError("Campo vacio");
        }
        else {
            resultado = Double.valueOf(data)*Double.valueOf(data);
            presenter.ShowResult(String.valueOf(resultado));
        }

    }
}
