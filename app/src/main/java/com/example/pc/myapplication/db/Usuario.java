package com.example.pc.myapplication.db;

import io.realm.Realm;
import io.realm.RealmObject;

/**
 * Created by PC on 17/03/2018.
 */

public class Usuario extends RealmObject
{


    private String login ="" ;
    private int id;
    private String avatar_url  ="";
    private String name = "" ;
    private String UltimoUsuario="";


    public Usuario()
    {

    }

    public String getUltimoUsuario() {
        return UltimoUsuario;
    }

    public void setUltimoUsuario(String UltimoUsuario) {
        this.UltimoUsuario = UltimoUsuario;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
