package com.example.pc.myapplication.di;

/**
 * Created by PC on 17/03/2018.
 */

public interface AlCuadrado
{
    interface View
    {
        void ShowResult(String result);
        void ShowError(String error);
    }

    interface Presenter
    {
        void ShowResult(String result);
        void AlCuadrado(String data);
        void showError(String error);
    }

    interface Model
    {
        void Alcuadrado(String data);
    }
}
