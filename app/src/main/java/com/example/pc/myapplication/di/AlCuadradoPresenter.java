package com.example.pc.myapplication.di;

import com.example.pc.myapplication.db.AlCuadradoModel;
import com.example.pc.myapplication.di.AlCuadrado;

/**
 * Created by PC on 17/03/2018.
 */

public class AlCuadradoPresenter implements AlCuadrado.Presenter {

    private AlCuadrado.View view;
    private AlCuadrado.Model model;

    public AlCuadradoPresenter(AlCuadrado.View view) {
        this.view = view;
        model = new AlCuadradoModel(this);

    }

    @Override
    public void ShowResult(String result)
    {
        if (view != null) {
            view.ShowResult(result);
        }

    }

    @Override
    public void AlCuadrado(String data)
    {
        if (view != null) {
            model.Alcuadrado(data);
        }
    }

    @Override
    public void showError(String error) {
        if (view != null) {
            view.ShowError(error);
        }
    }
}
