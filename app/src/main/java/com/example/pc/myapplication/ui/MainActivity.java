package com.example.pc.myapplication.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.net.Uri;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pc.myapplication.MyApplication;
import com.example.pc.myapplication.api.Servicio;
import com.example.pc.myapplication.db.Usuario;
import com.example.pc.myapplication.di.AlCuadrado;
import com.example.pc.myapplication.di.AlCuadradoPresenter;
import com.example.pc.myapplication.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import io.realm.Realm;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity implements AlCuadrado.View {


    private EditText txtBuscar;
    private ImageView imageView;
    private TextView tvResultado;
    private Realm MyRealm;
    String URLBase = "https://api.github.com/users";

    //private AlCuadrado.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //instancia de my real
        MyRealm = Realm.getDefaultInstance();

        tvResultado = (TextView) findViewById(R.id.tvResultado);
        txtBuscar = (EditText) findViewById(R.id.txtBuscar);
        imageView = (ImageView) findViewById(R.id.imageView);
        ConsularBD();

        try {
            //leer resultados base de datos
            RealmResults<Usuario> ultimoUsurioBuscado = MyRealm.where(Usuario.class).findAll();
            StringBuffer builder = new StringBuffer();


            for (Usuario user : ultimoUsurioBuscado) {

            if(user.getUltimoUsuario().toString().equals("v"))
            {

                tvResultado.setText(user.getName());
                Picasso.with(this)
                        .load(user.getAvatar_url())
                        .error(R.mipmap.ic_launcher)
                        .fit()
                        .centerInside()
                        .into(imageView);


                MyRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {



                        // realm.deleteAll();
                        //guardando en base de datos
                        Usuario user = MyRealm.where(Usuario.class).findFirst();



                        user.setUltimoUsuario("f");


                        Log.e("Editado : ", user.getUltimoUsuario().toString());

                    }
                });





            }

            }
        } catch (Exception e) {
            Log.e("Error: ", e.toString());
        }
    }

        //presenter = new AlCuadradoPresenter(this);




    String Imagen = "";
    String Nombre = "";
    String Usuario = "";
    String InicioSesion = "";

    public void Consular(View view) {
        {

            InicioSesion = txtBuscar.getText().toString();
/*
            MyRealm.beginTransaction();
            Usuario infoDB =  MyRealm.createObject(Usuario.class);
            infoDB.setUltimoUsuario("f");
            MyRealm.copyToRealm(infoDB); //instrucción para asegurar inserción
            MyRealm.commitTransaction();
*/

                View view2 = this.getCurrentFocus();
                view.clearFocus();
                if (view2 != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }


                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                URL url = null;

                HttpURLConnection conn;


                try {

                    if (InicioSesion.toString().equals("")) {


                        Toast.makeText(this, "Debes ingresar el usuario a buscar", Toast.LENGTH_SHORT).show();

                    } else if (ConsularBD(InicioSesion.toString()) != true) {
                        url = new URL(URLBase + "/" + txtBuscar.getText().toString());
                        conn = (HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("GET");

                        conn.connect();

                        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                        String inputLine;

                        StringBuffer response = new StringBuffer();

                        String json = "";

                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }

                        json = response.toString();

                        JSONArray jsonArr = null;

                        jsonArr = new JSONArray("[" + json + "] ");


                        for (int i = 0; i < jsonArr.length(); i++) {
                            JSONObject jsonObject = jsonArr.getJSONObject(i);
                            // User = jsonObject.optString("login");

                            if (Nombre != null) {
                                Usuario = jsonObject.optString("login");
                                Imagen = jsonObject.optString("avatar_url");
                                Nombre = jsonObject.optString("name");

                                tvResultado.setText(Nombre);
                                Picasso.with(this)
                                        .load(Imagen)
                                        .error(R.mipmap.ic_launcher)
                                        .fit()
                                        .centerInside()
                                        .into(imageView);



                                MyRealm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {

                                        // realm.deleteAll();
                                        //guardando en base de datos
                                        Usuario user = MyRealm.createObject(Usuario.class);


                                        user.setAvatar_url(Imagen);
                                        user.setName(Nombre);
                                        user.setLogin(Usuario);
                                        user.setUltimoUsuario("v");


                                        Log.e("Guardado URL: ", user.getAvatar_url());
                                        Log.e("Guardado URL: ", user.getName());
                                        Log.e("Guardado URL: ", user.getLogin());
                                        Log.e("Guardado URL: ", user.getUltimoUsuario().toString());

                                    }
                                });


                            }
                        }
                    } else {
                    /*try {
                        //leer resultados base de datos
                        RealmResults<Usuario> users = MyRealm.where(Usuario.class).findAll();
                        StringBuffer builder = new StringBuffer();
                        for (Usuario user : users)
                        {

                            tvResultado.setText(user.getName());
                            Picasso.with(this)
                                    .load(user.getAvatar_url())
                                    .error(R.mipmap.ic_launcher)
                                    .fit()
                                    .centerInside()
                                    .into(imageView);

                            builder.append("\n Usuario else: " + user.getLogin());
                            builder.append("\n Nombre: " + user.getName());
                            builder.append("\n Foto: " + user.getAvatar_url());

                        }
                        Log.e("TAG", builder.toString());

                    } catch (Exception e) {
                        Log.e("Error: ", e.toString());
                    }

                    // tvResultado.setText("Usuario encontrado");
                }





            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }*/
                    }


                    //  presenter.AlCuadrado(edAlCuadrado.getText().toString());
                } catch (Exception e) {

                }
                txtBuscar.setText("");
            }
        }

    private void CrearUsuario(final String login, final String avatar_url, final String name, final String estado) {

        try {
            MyRealm.beginTransaction();

            MyRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Usuario usuario = realm.createObject(Usuario.class);

                    usuario.setLogin(login);
                    usuario.setAvatar_url(avatar_url);
                    usuario.setName(name);
                    usuario.setUltimoUsuario(estado);


                    Log.e("Guardado avatar: ", usuario.getAvatar_url());
                    Log.e("Guardado name: ", usuario.getName());
                    Log.e("Guardado login: ", usuario.getName());
                    Log.e("Guardado estado: ", usuario.getUltimoUsuario().toString());
                }
            });
            MyRealm.commitTransaction();
            MyRealm.close();
        } catch (Exception e) {
            Log.e("Error guardando: ", e.getMessage());
        }
    }









    public boolean ConsularBD(String usuario) {
        try {
/*
            MyRealm.beginTransaction();
            Usuario infoDB =  MyRealm.createObject(Usuario.class);
            infoDB.setUltimoUsuario("f");
            MyRealm.copyToRealm(infoDB); //instrucción para asegurar inserción
            MyRealm.commitTransaction();
*/

            //leer resultados base de datos
            RealmResults<Usuario> users = MyRealm.where(Usuario.class).findAll();
            StringBuffer builder = new StringBuffer();
            for (Usuario user : users) {

                if (user.getLogin().toString().equals(usuario.toString()))
                {
                    tvResultado.setText(user.getName());
                    Picasso.with(this)
                            .load(user.getAvatar_url())
                            .error(R.mipmap.ic_launcher)
                            .fit()
                            .centerInside()
                            .into(imageView);
                    MyRealm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {



                            // realm.deleteAll();
                            //guardando en base de datos
                            Usuario user = MyRealm.where(Usuario.class).findFirst();



                            user.setUltimoUsuario("v");


                            Log.e("Editado : ", user.getUltimoUsuario().toString());

                        }
                    });




                    builder.append("\n Usuario Buscado: " + user.getLogin());
                    builder.append("\n Nombre: " + user.getName());
                    builder.append("\n Foto: " + user.getAvatar_url());
                    builder.append("\n Ultimo usuario: " + user.getUltimoUsuario()+"\n");
                    return true;
                }

            }
            Log.e("TAG", builder.toString());

        } catch (Exception e) {
            Log.e("Error: ", e.toString());
        }
        return false;
    }


    public void ConsularBD() {
        try {
            //leer resultados base de datos
            RealmResults<Usuario> users = MyRealm.where(Usuario.class).findAll();
            StringBuffer builder = new StringBuffer();
            for (Usuario user : users)
            {


                builder.append("\n Usuario Buscado: " + user.getLogin());
                builder.append("\n Nombre: " + user.getName());
                builder.append("\n Foto: " + user.getAvatar_url());
                builder.append("\n Ultimo usuario: " + user.getUltimoUsuario());
            }
            Log.e("TAG", builder.toString());

        } catch (Exception e) {
            Log.e("Error: ", e.toString());
        }

    }
    @Override
    protected void onDestroy() {


        try {
/*
            MyRealm.beginTransaction();
            Usuario infoDB =  MyRealm.createObject(Usuario.class);
            infoDB.setUltimoUsuario("f");
            MyRealm.copyToRealm(infoDB); //instrucción para asegurar inserción
            MyRealm.commitTransaction();
*/

            //leer resultados base de datos
            RealmResults<Usuario> users = MyRealm.where(Usuario.class).findAll();
            StringBuffer builder = new StringBuffer();
            for (Usuario user : users) {

                if (user.getLogin().toString() != InicioSesion.toString()) {

                    MyRealm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            // realm.deleteAll();
                            //guardando en base de datos
                            Usuario user = MyRealm.where(Usuario.class).findFirst();
                            user.setUltimoUsuario("f");
                            Log.e("Editado : ", user.getUltimoUsuario().toString());
                        }
                    });


                }
            }
        }catch (Exception e)
        {

        }
        super.onDestroy();
        MyRealm.close();
    }

    @Override
    public void ShowResult(String result) {
        tvResultado.setText(result);
    }

    @Override
    public void ShowError(String error) {
        tvResultado.setText(error);
    }
}
